<?php
namespace App\SummaryOfOrganization;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;


class SummaryOfOrganization extends DB{
    public $id="";
    public $organizationName="";
    public $summaryOfOrganization="";

    public function __construct(){



        parent::__construct();

    }
    public function index(){
        echo "";
        echo "<br>";
        echo $this->organizationName;
    }



    public function setData($postVariabledata=NULL){

        if(array_key_exists('id',$postVariabledata)){

            $this->id=$postVariabledata['id'];
        }

        if(array_key_exists('organization_name',$postVariabledata)){
            $this->organization_Name=$postVariabledata['organization_name'];
        }

        if(array_key_exists('organization_summary',$postVariabledata)){
            $this->summaryOfOrganization=$postVariabledata['organization_summary'];
        }
       // var_dump($this);die();
    }

    public function store(){

        $arrData=array( $this->organization_Name, $this->summaryOfOrganization);
        $sql="INSERT INTO summary_of_organization(organization_name, organization_summary) VALUES (?,?)";

       // var_dump($sql);die();
        // $sql="insert INTO book_title(book_title, author_name) VALUES ('$this->book_title', '$this->author_name')"; // for stop SQL Injection Hacking we need to use value(?,?)

        $STH=$this->DBH->prepare($sql);    // $sql returning object in $STH. prepare method returns object.
       /* $STH->bindParam(1, $this->organization_Name);
        $STH->bindParam(2,  $this->summaryOfOrganization);*/

        //$STH->execute();
        $result=$STH->execute($arrData);

        if($result)
            Message::Message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::Message("Failed! Data Has not Been Inserted Successfully :( ");

        Utility::redirect('create.php');


    }// end of store method

}